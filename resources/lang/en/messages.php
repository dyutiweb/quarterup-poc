<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Message Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'profile_update' => [
		'error'   => 'Could not save your profile information!',
		'success' => 'Your profile updated successfully!',
		'duplicate_email' => 'The email you provided is being used by some other user!',
		'duplicate_username' => 'The username you provided is being used by some other user!',
	],	

	'skill_create' => [
		'error'   => 'Could not save your skill information!',
		'success' => 'Your skill created successfully!',
	],	

	'skill_remove' => [
		'error'   => 'Could not remove your skill information!',
		'success' => 'Your skill removed successfully!',
	],	

];