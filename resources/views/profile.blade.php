@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Profile</div>

				<div class="panel-body">
					
					{{ notify_display() }}

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/update') }}" 
					enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ $user->name }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Gender</label>
							<div class="col-md-6">
								<input type="radio" name="gender" value="male" @if ('male' == $user->gender) checked @endif> Male
								<input type="radio" name="gender" value="female" @if ('female' == $user->gender) checked @endif> Female
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Birth Date</label>
							<div class="col-md-6">
								<input type="text" name="birth_date" id="birth_date" class="form-control" placeholder="MM/DD/YYYY" 
								@if( !empty($user->birth_date)) value="{{ date('m/d/Y', strtotime($user->birth_date)) }}" @endif/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ $user->email }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Username</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="username" value="{{ $user->username }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-6 control-label"><b>Change Password</b> (Leave empty if not changing)</label>	
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Photo</label>
							<div class="col-md-6">
								@if( !empty($user->avatar)) 
								<div class="col-xs-6 col-md-3">
									<div class="thumbnail">
									<img src="{{ \Config::get('quarterup.uploads_dir.photo'). '/' . 'thumb_' . $user->avatar }}" />
									</div>
								</div>
								@endif
								<input type="file" name="photo">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Update
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
      jQuery(document).ready(function(){
        //initialize the javascript
        
        jQuery("#birth_date").mask("99/99/9999");
      });
    </script>
@endsection
