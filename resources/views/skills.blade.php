@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Skills</div>

				<div class="panel-body">
					<ul class="list-group" id="skills">
					  @if( $user->skills->count() > 0 )

					  @foreach( $user->skills as $skill)
					  <li class="list-group-item" id="skill_{{ $skill->id }}">
					    <div class="col-lg-9">
					    	{{ $skill->skill_name }}				    	
					    </div>	
					    <div class="col-lg-2">
					    	<span class="label label-default">
					    		{{ $skill->skill_level }}	
					    	</span>	
					    	<a class="glyphicon glyphicon-remove pull-right" role="button" data-skill="{{ $skill->id }}" 
					    	href="javascript://"></a>
					    </div>		
					    <div class="clearfix"></div>
					  </li>
					  @endforeach
					  @endif
					</ul>
					<ul class="list-group">
						<li class="list-group-item">
					  	<form method="post" name="frm_skill_new" id="frm_skill_new">
					  		<input type="hidden" name="" value="">
						  	<div>
						  		<div class="col-lg-6">
						    		<input type="text" class="form-control" name="skill_name" id="skill_name">
						    	</div>
						    	<div class="col-lg-3">
							    	<select class="form-control" name="skill_level" id="skill_level">
							    		<option value="low">low</option>
							    		<option value="mederate">mederate</option>
							    		<option value="good">good</option>
							    		<option value="excellent">excellent</option>
							    	</select>
						    	</div>
						    	<div class="col-lg-3">
						    		<button type="button" class="btn btn-primary btn-am" onclick="skill_new()">
									  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New
									</button>
						    	</div>
						    </div>
						</form>    
					    <div class="clearfix"></div>	
					  </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){

		skill_new= function(){
			jQuery.ajax({
				url: '{{ url('/skills/new') }}',
				type: 'post',
				dataType: 'json',
				data: { skill_name: jQuery('#skill_name').val(), skill_level: jQuery('#skill_level').val(), 
				        _token: '{{ csrf_token() }}' },
				beforeSend: function(xhr){

				},
				success: function(resp){
					if( resp.status == 'success' ){
						row = jQuery('<li class="list-group-item" id="skill_'+resp.skill.id+'"/>');
						div1 = jQuery('<div class="col-lg-9"/>');
						
						div1.html( resp.skill.skill_name );
						
						row.append( div1 );

						div2 = jQuery('<div class="col-lg-2"/>');						

						sp	= jQuery('<span class="label label-default"/>');
						
						sp.html( resp.skill.skill_level )
						
						ah	= jQuery('<a class="glyphicon glyphicon-remove pull-right" role="button" '+
						             'data-skill="'+resp.skill.id+'" href="javascript://"/>');

						div2.append(sp);
						div2.append(ah);

						row.append( div2 );

						div3 = jQuery('<div class="clearfix"/>');
						row.append( div3 );

						jQuery('#skills').append( row );

						jQuery('#frm_skill_new')[0].reset();

						skill_events()
					}
				}
			});
		}	

		skill_remove=function(id){
			jQuery.ajax({
				url: '{{ url('/skills/remove') }}',
				type: 'post',
				dataType: 'json',
				data: { id: id, _token: '{{ csrf_token() }}' },
				beforeSend: function(xhr){

				},
				success: function(resp){
					if( resp.status == 'success' ){
						jQuery('#skills').find("li#skill_"+id).fadeOut('slow', function(){
							jQuery('#skills').find("li#skill_"+id).remove();
						});
					}
				}
			});
		}

		skill_events=function(){
			jQuery('#skills').find("a[data-skill]").bind('click', function(){
				skill_remove( jQuery(this).attr('data-skill') )
			})
		}

		skill_events();
	});
</script>
@endsection
