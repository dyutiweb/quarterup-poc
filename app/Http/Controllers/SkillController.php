<?php namespace App\Http\Controllers;

class SkillController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Skill Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		// user
		$user = \Auth::getUser();

		// view
		return view('skills')->with('user', $user);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function create()
	{
		// nit
		$errors = ['status'=>'error', 'message'=>trans('messages.skill_create.error')];

		// user
		$user = \Auth::getUser();

		try{
			// post
			$post = \Request::all();

			// skill data
			$skill_data = [];
			foreach(['skill_name','skill_level'] as $field){
				if( isset($post[$field]) ){
					$skill_data[$field] = $post[$field];
				}
			}

			// save
			$skill = $user->skills()->save( new \QuarterUp\Model\UserSkill( $skill_data ) );

			// saved
			if( isset($skill->id) ){
				// set
				$errors['status']  = 'success';
				$errors['message'] = trans('messages.skill_create.success');
				$errors['skill']   = $skill->toArray();
			}
		}catch (Exception $e){
		// log	
			\Log::debug( $e->getMessage(), ['context'=>'skill_create']);

			// response
			$errors['message'] .= $e->getMessage();
		}	

		// ajax
		if( \Request::ajax() ){
		// ok	
			return response()->json( $errors );
		}

		return redirect('skills')->with('errors', $errors);		
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function remove()
	{
		// nit
		$errors = ['status'=>'error', 'message'=>trans('messages.skill_remove.error')];

		// user
		$user = \Auth::getUser();

		try{
			// post
			$post = \Request::all();

			// save
			$success = $user->skills()->where('id', '=', $post['id'])->delete();

			// saved
			if( $success ){
				// set
				$errors['status']  = 'success';
				$errors['message'] = trans('messages.skill_remove.success');
			}
		}catch (Exception $e){
		// log	
			\Log::debug( $e->getMessage(), ['context'=>'skill_remove']);

			// response
			$errors['message'] .= $e->getMessage();
		}	

		// ajax
		if( \Request::ajax() ){
		// ok	
			return response()->json( $errors );
		}

		return redirect('skills')->with('errors', $errors);		
	}
}
