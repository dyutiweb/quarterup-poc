<?php namespace App\Http\Controllers;

class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Profile Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		// user
		$user = \Auth::getUser();

		// view
		return view('profile')->with('user', $user);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function update()
	{
		// nit
		$errors = ['status'=>'error', 'message'=>trans('messages.profile_update.error')];
		
		try{

			// user
			$user = \Auth::getUser();

			// fields to update
			$fields_to_update = ['name','email','username','gender','birth_date'];

			if( \Request::has('password') && \Request::has('password_confirmation') ){
				if( \Request::get('password') == \Request::get('password_confirmation') ){
					$fields_to_update[] = 'password';
				}
			}

			// loop
			foreach( $fields_to_update as $field ){
				$field_value = \Request::get($field);				

				if( 'birth_date' == $field ){
					if( ! empty($field_value) ){
						$user->$field = date('Y-m-d', strtotime($field_value));
					}
				}elseif( 'password' == $field ){
					$user->$field = bcrypt($field_value);
				}else{
					$user->$field = $field_value;
				}
			}

			if (\Request::hasFile('photo')){

				$photo = \Request::file('photo');

				if( starts_with($photo->getMimeType(), 'image/') ){
					// dest
					$destination = \Config::get('quarterup.uploads_dir.photo');

					// old 
					if( ! empty($user->avatar) && \Storage::exists( public_path( $destination . '/' . $user->avatar) ) ){
						$old_image = $user->avatar;
					}

					$token = create_token(8);
					$ext   = $photo->getClientOriginalExtension();

					$filename  = $token . '.' . $ext;
					$thumbnail = 'thumb_' . $filename;
				    
				    $photo->move($destination, $filename);

				    $user->avatar = $filename;

				    // tumb
				    \Image::make($destination . '/' . $filename, [
					    'width' => 100,
					    'height' => 100
					])->save( $destination . '/' . $thumbnail);
				}
			}

			// check username and email
			if( \QuarterUp\Model\User::where('email','=', $user->email)->where('id','!=', $user->id)->count() > 0 ){
				$errors['message'] = trans('messages.profile_update.duplicate_email');
			}elseif( \QuarterUp\Model\User::where('username','=', $user->username)->where('id','!=', $user->id)->count() > 0 ){
				$errors['message'] = trans('messages.profile_update.duplicate_username');
			}else{
				// save
				if( $user->save() ){
					$errors = ['status'=>'success', 'message'=>trans('messages.profile_update.success')];

					// check
					if( isset($old_image) && !empty($old_image) ){
						$main_photo = public_path( $destination . '/' . $old_image );
						if( \Storage::exists( $main_photo ) ){
							\Storage::delete( $main_photo );
						}

						$thumb_photo = public_path( $destination . '/' . 'thumb_' . $old_image );
						if( \Storage::exists( $thumb_photo ) ) {
							\Storage::delete( $thumb_photo );
						}
					}
				}	
			}			
		
		}catch (Illuminate\Database\QueryException $e){
			$errors['message'] .= ' '. $e->getMessage();
		}catch (Exception $e){
			$errors['message'] .= ' '. $e->getMessage();
		}	

		return redirect('profile')->with('errors', $errors);
	}

}
