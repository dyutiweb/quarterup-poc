<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('dashboard', 'DashboardController@index');

Route::post('profile/update', 'ProfileController@update');

Route::get('profile', 'ProfileController@index');

Route::post('skills/new', 'SkillController@create');

Route::post('skills/remove', 'SkillController@remove');

Route::get('skills', 'SkillController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Route::get('/', 'WelcomeController@index');

Route::get('/', 'Auth\AuthController@getLogin');