<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenderToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
			$table->string('username', 50)->unique()->after('email');	
		    $table->enum('gender', ['male','female'])->after('password');		    	    
		    $table->date('birth_date')->nullable()->after('gender');		    
		    $table->string('avatar', 100)->nullable()->after('birth_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($table)
		{	
			$table->dropUnique('users_username_unique');

		    $table->dropColumn(['username', 'gender', 'birth_date','avatar']);
		});
	}

}
