<?php 
namespace QuarterUp\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * User Model
 * Store users of site
 * 
 * @package QuarterUp
 * @subpackage Laravel
 * 
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract{
	use Authenticatable, CanResetPassword, EntrustUserTrait;
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Fillable
	 * 
	 * @var array
	 */ 
	protected $fillable = ['name', 'email', 'username', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * Skills
	 * 
	 * @return array
	 */ 
	public function skills()
	{
		return $this->hasMany('\QuarterUp\Model\UserSkill', 'user_id');
	}
}