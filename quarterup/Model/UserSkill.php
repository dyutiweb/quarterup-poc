<?php
namespace QuarterUp\Model;

/**
 * UserBookmark Model
 * Store user bookmarks
 * 
 * @package QuarterUp
 * @subpackage Laravel
 * 
 */
class UserSkill extends \Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_skills';

	/**
	 * Fillable
	 * 
	 * @var array
	 */ 
	protected $fillable = ['skill_name','skill_level'];


	/**
	 * Relationship to User
	 */
	public function user()
    {
        return $this->belongsTo('\QuarterUp\Model\User');
    }
}