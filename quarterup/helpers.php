<?php
/*
|--------------------------------------------------------------------------
| Application Helpers
|--------------------------------------------------------------------------
|
| Defined helpers for the Application.
|
*/

if ( ! function_exists('pr') )
{
	/**
	 * recursive dump
	 *
	 * @param mixed $var
	 * @param bool $return
	 * @return string
	 */
	function pr($var, $return=false, $html=true){	
		// pattern
		$pattern = $html ? '<pre>%s</pre>' : '%s';
		// output
		$output = sprintf( $pattern, print_r($var, true) );
		// return 
		if($return) return $output;
		// print
		echo $output;
	}
}	

if ( ! function_exists('debug_log') )
{
	/**
	 * debug log
	 * 
	 * @param mixed $content
	 * @param string $name
	 * @param string $ext
	 * @return string
	 */
	function debug_log($content, $name='debug', $ext='log'){
		// convert
		if(is_array($content) || is_object($content)){
			$content = pr($content, true);
		}
		// content
		$content .= "\n\n";
		// difine time
		if( ! defined('LOG_TIMESTAMP')) define('LOG_TIMESTAMP', date('Ymd_His'));
		// write
		return File::append(storage_path(sprintf('logs/%s_%s.%s', $name, LOG_TIMESTAMP, $ext)), $content);	
	}
}

if ( ! function_exists('notify_key') )
{
	/**
	 * get notify key
	 * 
	 * @param array $notify
	 * @param string $group
	 * @return void
	 */
	function notify_key( $group=null ){
		// group
		if( ! $group ){
			$key = 'errors';
		}else{
			$key = 'errors_' . lower_case( $group );
		}
		// return
		return $key;
	}
}

if ( ! function_exists('notify_set') )
{
	/**
	 * set notify
	 * 
	 * @param array $notify
	 * @param string $group
	 * @return void
	 */
	function notify_set( $notify, $group=null ){	
		// key 
		$key = notify_key( $group ) ;
		// set
		\Session::flash( $key, $notify );
	}
}

if ( ! function_exists('notify_get') )
{
	/**
	 * get notify
	 * 
	 * @param string $group
	 * @return array $notify
	 */
	function notify_get( $group=null ){
		// key 
		$key = notify_key( $group ) ;

		// notify
		if ( \Session::has( $key ) ) {
			$notify = \Session::pull( $key );
		
			// check
			if( isset($notify['status']) )
				return $notify;			
		}
		
		// return
		return false;
	}
}

if ( ! function_exists('notify_has') )
{
	/**
	 * has notify
	 * 
	 * @param string $group
	 * @return bool
	 */
	function notify_has( $group=null ){
		// key 
		$key = notify_key( $group ) ;

		// return		
		return \Session::has( $key );			
	}
}

if ( ! function_exists('notify_display') )
{
	/**
	 * display notify
	 * 
	 * @param string $group
	 * @return void
	 */
	function notify_display( $group=null ){	
		// check
		if( $notify = notify_get( $group ) ){
			
			// classes
			$classes = array('success'=>'success','error'=>'danger','info'=>'info','warning'=>'warning');
			// latout
			$data = array('class'=>$classes[$notify['status']], 'message'=>$notify['message']);
			// show
			print View::make( 'layouts.notify.default', $data);			
		}
	}
}	


if ( ! function_exists('create_token'))
{
    /**
	 * Create token
	 *
	 * @param int $size
	 * @return string
	 */
	function create_token( $size=6 ){
		return lower_case( \Illuminate\Support\Str::random( $size ) );
	}
}

if ( ! function_exists('lower_case'))
{
	/**
	 * Convert a value to lower case.
	 *
	 * @param  string  $value
	 * @return string
	 */
	function lower_case($value)
	{
		return \Illuminate\Support\Str::lower($value);
	}
}

if ( ! function_exists('title_case'))
{
	/**
	 * Convert a value to lower case.
	 *
	 * @param  string  $value
	 * @return string
	 */
	function title_case($value)
	{
		return \Illuminate\Support\Str::title($value);
	}
}


if ( ! function_exists('last_query'))
{
	/**
	 * last query 
	 *
	 * @param void
	 * @return string $last_query 
	 * @since 1.0.0
	 */
	 function last_query( $print=false ){
		$queries = \DB::getQueryLog();
		$last = end( $queries );

		// print
		if( $print ){
			return pr($last);
		}

		return $last;		
	}
}	

if ( ! function_exists('bool_from_yn') )
{
	/**
	 * Alias Split from database select column
	 * 
	 * @param string $alias
	 * @return string $alias
	 */ 
	function bool_from_yn( $yn ){
		return starts_with($yn, 'y');
	}
}